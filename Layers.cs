﻿using UnityEngine;
using System.Collections;

public partial class Layers
{
	public static LayerMask All { get{ return -1; } }
	
	// Returns whether the given layer is inside the given layermask
	public static bool IsInLayerMask (int layer, LayerMask mask)
	{
		return ((mask >> layer) & 1) == 1;
    }
	
	// Creates a layermask out of the given layers
	public static LayerMask MaskOnly (params int[] layers)
	{
		LayerMask mask = 0;
		foreach(var l in layers)
		{
			mask |= l;
		}
		return mask;
	
	}
	
	// Creates a layermask excluding the given layers
	public static LayerMask MaskExcept (params int[] layers)
	{
		return ~MaskOnly(layers);
	}
}

public static class LayerMaskExtensions
{
	public static bool Includes (this LayerMask mask, int layer)
	{
		return Layers.IsInLayerMask(layer, mask);
	}
}
