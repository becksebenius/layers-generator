Layers Generator
============
Allows you to generate a Layers.cs file to keep your layer referencing type-safe.
Use Assets/Autogenerate/Layers to generate the layers file.