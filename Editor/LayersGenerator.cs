﻿using UnityEngine;
using UnityEditor;
using System;
using System.Text;
using System.IO;
using System.Linq;

public class LayersEditor
{	
	[MenuItem("Assets/Autogenerate/Layers")]
	public static void CreateLayers ()
	{
		//Search for layers manually since by default Layers doesn't exist
		Type type = null;
		foreach(var assembly in AppDomain.CurrentDomain.GetAssemblies())
		{
			var t = assembly.GetType("Layers", false);
			if(t != null) type = t;
		}
		
		//Create an instance of Layers so we can find it's monoscript
		ScriptableObject layersInstance = null;
		
		if(typeof(ScriptableObject).IsAssignableFrom(type))
		{
			layersInstance = ScriptableObject.CreateInstance(type);
		}
		
		MonoScript script = null;
		if(layersInstance)
		{
			script = MonoScript.FromScriptableObject(layersInstance);
		}
		
		string path = null;
		if(script)
		{
			//get path from monoscript
			path = AssetDatabase.GetAssetPath(script);
		}
		else
		{
			// if the monoscript doesn't exist, we want to save to the default location
			path = EditorUtility.SaveFilePanel("Save Layers.cs", "", "Layers", "cs");
			if(path == "" || path == null)
			{
				return;
			}
			
			var fileInfo = new FileInfo(path);
			
			if(fileInfo.Name != "Layers.cs")
			{
				Debug.LogError("File must be named \"Layers.cs\"");
				return;
			}
		}
		
		//Generate the script
		string layersScript = GenerateLayersScript();
			
		//Write the script onto the file
		File.WriteAllText(path, layersScript);
		
		//Refresh the asset database so that it knows to reimport
		AssetDatabase.Refresh();
	}
	
	delegate void AddLineDelegate (params object[] text);
	public static string GenerateLayersScript ()
	{
		var builder = new StringBuilder();
		
		int tab = 0;
		
		// Avoid string concat
		AddLineDelegate addLine = (text)=>{
			for(int i = 0; i < tab; i++) builder.Append("\t");
			foreach(var s in text) builder.Append(s.ToString());
			builder.Append("\n");
		};
		
		//headers
		addLine("using UnityEngine;");
		addLine();
		
		//class definition
		addLine("public partial class Layers : ScriptableObject");
		addLine("{");
		addLine();
		
		tab++;
		
		//log out the layers
		for(int i = 0; i < 32; i++)
		{
			//grab layer name from project
			var name = LayerMask.LayerToName(i);
			
			//don't create the variable if the layer isn't in use
			if(name == "") continue;
			
			//replace spaces with underscores
			name = name.Replace(" ", "_");
			
			//add the line
			addLine("public static int ", name, " = ", i, ";");
			addLine();
		}
		
		addLine();
		
		tab--;
		addLine("}");	// class Layers
		
		//concat the string
		string result = builder.ToString();
		
		return result;
	}
}